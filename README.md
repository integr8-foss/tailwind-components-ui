# tailwind-components-ui

For the components available on https://tailwindcomponents.com the idea is to make available vue and react components created such a way that any one can directly use the component in their projects. The plan is to then expand it with more components